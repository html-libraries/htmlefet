function termplate(evalWithContext, templateCb, resolverFunctionName = null) {
    let templateCbCode = templateCb.toString();
    if (resolverFunctionName) {
        let firstBacktickIndex = templateCbCode.indexOf("`");
        templateCbCode =
            templateCbCode.substring(0, firstBacktickIndex) +
            resolverFunctionName +
            templateCbCode.substring(firstBacktickIndex);
    }
    let newTemplateCode = convertExpressionsToFunctions(templateCbCode);
    return evalWithContext(newTemplateCode)();
}

function convertExpressionsToFunctions(templateCode) {
    let expressionNestingCounter = 0;
    let lastExpressionEndIndex = 0;
    let lastExpressionStartIndex = -1;
    let parenthesesInsideExpressionCounter = 0; // For functions and objects inside expression

    // TODO quotes
    let quoteChars = "\"'`";
    let quotesNestingCounter = 0;

    let newCode = "";

    for (let i = 0; i < templateCode.length; i++) {
        let currentString = templateCode.substring(i);

        // Check for expression markers only if not inside quotes
        if (quotesNestingCounter === 0) {
            if (parenthesesInsideExpressionCounter === 0 || expressionNestingCounter === 0) {
                if (currentString.startsWith("${")) {
                    expressionNestingCounter += 1;
                    if (expressionNestingCounter === 1) {
                        lastExpressionStartIndex = i;
                        newCode += templateCode.substring(lastExpressionEndIndex, i);
                    }
                } else if (currentString.startsWith("}")) {
                    expressionNestingCounter -= 1;
                    if (expressionNestingCounter === 0) {
                        lastExpressionEndIndex = i;
                        newCode += `\$\{() => ${templateCode.substring(lastExpressionStartIndex + 2, i)}`;
                    }
                } else if (currentString.startsWith("{") && (i === 0 || templateCode[i - 1] !== "$")) {
                    // Check for parentheses inside expressions
                    parenthesesInsideExpressionCounter += 1
                }
            } else if (currentString.startsWith("}")) {
                // Resolve parentheses inside expressions
                parenthesesInsideExpressionCounter -= 1
            }
        }
    }

    newCode += templateCode.substring(lastExpressionEndIndex);
    return newCode
}


export default termplate
/**
 * A bound DomNode contains one or more expressions, and each expression contains one or more props. a prop can be
 * linked to more than one expression.
 *
 *           +--> Expression ---> Prop
 *           |                /
 * DomNode --|--> Expression ---> Prop
 *           |
 *           +--> Expression ---> Prop
 *
 * ## prop updates
 * -> expressions linked to prop are set as shouldEvaluate
 * -> all shouldEvaluate expressions are evaluated and the returned values stored
 * -> domNodes linked to expression are set as shouldUpdate
 * -> all shouldUpdate domNodes update
 *
 * A DomNode's value can consist of multiple expressions and some static text.
 * expression
 */

/** TODO
 * name: bindhtml? HTML EFficient ETernally / ETcetera
 * merge htmlau into HTMLefetTemlate and make code prettier and smaller and faster and stronker
 * props watcher: big big problem. ctrl-f "re-evaluation won't be queued" here
 * print prop to expression map + expression to domNode map. basically, print the whole template process...
 * spread attributes (list of attributes, as dict?)
 * make guide
 *      - in the guide, include react example like here: https://github.com/developit/htm
 * check if can partially update text node: read about Range
 * Profile memory: do we leak? especially watcher
 * Partial CSS update. this is possible:
 *      - $0.sheet.cssRules[1].style.cssText = "background-color: blue;"
 *      - $0.sheet.cssRules[1].style.backgroundColor = "red"
 *
 *      use cases to cover:
 *      - property value (green),
 *      - property name (color),
 *      - whole css line (color: green;)
 *      - whole css selector content (color: green; font-size: 24px;)
 *      - css selector (.fakio>#ikaramba)
 *      - whole css rule (selector + content) / multiple rules
 *      All of these must work together!!!
 * Make guide + test run by jonathan, stav, and then gilad
 * HTMLefetElement, like litElement
 * change render interval back to 60 fps + benchmark performance
 * event binding: need to add event name, event name + value
 * lodash: only import throttle...
 */

import watch from "./libs/objectWatcher.js"
import termplate from "./libs/termplate.js"
import "./libs/lodash.min.js"


const DOMNodeFinder = new class {
    // TODO: Change to node types ($0.nodeType) like in the browser
    NODE_TYPES = {
        TEXT_NODE: "Text/CSS node",
        ATTR_VALUE: "Attribute value",
        ATTR_NAME: "Attribute name",
        HTML_ELEMENT: "HTML Element"
    };

    find(element, value) {
        const xpathSearchers = [
            {
                xpath: `.//text()[contains(., '${value}')]`,
                resolver: textNode => {
                    const parentElement = textNode.parentNode;
                    const value = textNode.data;

                    console.log(`Found ${value} in textNode. node: `, textNode);
                    console.log(`Containing element: `, parentElement);

                    return {
                        domNode: textNode,
                        type: this.NODE_TYPES.TEXT_NODE,
                        value: value,
                        updateDomNodeCb: newText => textNode.data = newText
                    }
                }
            }, {
                xpath: `.//@*[contains(., '${value}')]`,
                resolver: attributeNode => {
                    const attrName = attributeNode.name;
                    const attrValue = attributeNode.value;
                    const ownerElement = attributeNode.ownerElement;

                    console.log(`Found ${value} in attribute value. node: `, attributeNode);
                    console.log(`Containing element: `, ownerElement);

                    return {
                        domNode: attributeNode,
                        type: this.NODE_TYPES.ATTR_VALUE,
                        value: attrValue,
                        updateDomNodeCb: newAttributeValue => attributeNode.value = newAttributeValue
                    }
                }
            }, {
                xpath: `.//@*[contains(name(), '${value}')]`,
                resolver: attributeNode => {
                    let attrName = attributeNode.name;
                    const attrValue = attributeNode.value;
                    const ownerElement = attributeNode.ownerElement;

                    console.log(`Found ${value} in attribute name. node: `, attributeNode);
                    console.log(`Containing element: `, ownerElement);

                    return {
                        domNode: attributeNode,
                        type: this.NODE_TYPES.ATTR_NAME,
                        value: attrName,
                        updateDomNodeCb: newAttributeName => {
                            ownerElement.removeAttribute(attrName);
                            if (newAttributeName != null && newAttributeName !== "") {
                                ownerElement.setAttribute(newAttributeName, "");
                                attrName = newAttributeName;
                            }
                        }
                    }
                }
            }, {

                xpath: `.//${value}`,
                resolver: elementNode => {
                    console.log(`Found ${value} in HTMLElement. node: `, elementNode);
                    console.log(`Containing element: `, elementNode.parentNode);
                    let lastAttachedElement = elementNode;
                    return {
                        domNode: elementNode,
                        type: this.NODE_TYPES.HTML_ELEMENT,
                        value: null,
                        updateDomNodeCb: newHtmlElement => {
                            lastAttachedElement.replaceWith(newHtmlElement);
                            lastAttachedElement = newHtmlElement;
                            console.log("Updating HTMLefet element node (shouldnt happen more than once). new element: ", newHtmlElement)
                        }
                    }
                }
            }
        ];

        for (let searcher of xpathSearchers) {
            let result = document.evaluate(
                searcher.xpath,
                element,
                null, XPathResult.FIRST_ORDERED_NODE_TYPE, null
            ).singleNodeValue;
            if (result != null) {
                return searcher.resolver(result);
            }
        }
    }
};

class Expression {
    /**
     * A single expression inside an HTMLefeTemplate.
     */

    constructor(id, expressionCb) {
        this.id = id;
        this.cb = expressionCb;
        this.lastResult = null;
        this.boundNode = null;
    }

    execute() {
        this.lastResult = this.cb();
        return this.lastResult
    }
}

class BoundNode {
    /**
     * Contains a DOM Node and a list of expressions whose values the node contains
     */

    static _domNodeToBoundNode = new Map();

    static CreateBoundNode(element, expression) {
        const nodeData = DOMNodeFinder.find(element, expression.id);
        if (!nodeData) {
            console.error(`No dom node was found for expression id ${expression.id} in `, element)
        }
        let {domNode, type, value, updateDomNodeCb} = nodeData;

        // Text nodes are separated into node-per-expression, in case expression returns an element
        if (type === DOMNodeFinder.NODE_TYPES.TEXT_NODE && domNode.parentElement.localName !== "style") {
            /**
             * Value can be one of the following:
             *      - primitive value (string, number)
             *      - list of primitive values
             *      - element
             *      - list of elements
             *      - TODO: object / list of objects?
             */

            domNode = BoundNode._breakUpTextNodeToSmallerNodes(domNode, expression.id);
            let lastValue = "";
            let arrayDomNodes = [];
            return new BoundNode(
                () => {
                    let newValue = expression.lastResult;

                    // TODO: Deal with typeof === "function" and promises
                    if (lastValue instanceof Array) {
                        // Delete old array, make domNode the last remaining value
                        for (let domNodeToRemove of arrayDomNodes) {
                            if (domNodeToRemove !== domNode) {
                                domNodeToRemove.remove()
                            }
                        }
                        arrayDomNodes = [];
                    }

                    if (newValue instanceof Array) {
                        // Deal with array
                        arrayDomNodes = newValue.map(val => typeof val === "object" ? val : document.createTextNode(val));
                        let lastDomNodeInChain = domNode;
                        for (let domNodeToAdd of arrayDomNodes) {
                            BoundNode._insertAfter(domNodeToAdd, lastDomNodeInChain);
                            lastDomNodeInChain = domNodeToAdd;
                        }
                        domNode.remove();
                        domNode = arrayDomNodes[0];
                    } else if (typeof newValue === "object") {
                        // Deal with element
                        // TODO: Check if insranceof HTMLElement
                        domNode.replaceWith(newValue);
                        domNode = newValue;
                        console.log("##### Override object with element")
                    } else {
                        if (typeof lastValue === "object") {
                            // Handle an object becoming a string
                            let newTextNode = document.createTextNode(newValue);
                            domNode.replaceWith(newTextNode);
                            domNode = newTextNode;
                            console.log("##### Convert object to string: Created new text node")
                        } else {
                            domNode.data = newValue;
                            console.log("##### Set normal textNode value")
                        }
                    }

                    lastValue = newValue;
                },
                []
            );
        } else { // Attr name, Attr value and style are calculated as string templates
            let boundNode = BoundNode._domNodeToBoundNode.get(domNode);
            if (boundNode == null) {
                let attachedEvent = null;
                boundNode = new BoundNode(
                    expressions => {
                        if (expressions.length === 1 && typeof expressions[0].lastResult === "function") {
                            if (attachedEvent == null) {
                                let eventName = domNode.name.substring(2); // Remove the `on` from `onclick`
                                domNode.ownerElement.addEventListener(eventName, (...args) => attachedEvent(...args));
                                domNode.ownerElement.removeAttributeNode(domNode);
                                updateDomNodeCb();
                            }
                            attachedEvent = expression.lastResult;
                        } else {
                            // Replaces ids with expression values
                            let newValue = value;
                            for (let expression of expressions) {
                                newValue = newValue.replace(expression.id, expression.lastResult)
                            }

                            // Attribute name cant be empty
                            if (type === DOMNodeFinder.NODE_TYPES.ATTR_NAME && (newValue === "" || newValue == null)) {

                            }
                            // TODO: Save last domValue and only update if the value changed. spare dom updates yay
                            updateDomNodeCb(newValue);
                        }
                    },
                    [expression]
                );
                BoundNode._domNodeToBoundNode.set(domNode, boundNode);
            } else {
                boundNode.expressions.push(expression)
            }
            return boundNode;
        }
    }

    static _breakUpTextNodeToSmallerNodes(textNode, textToMakeNode) {
        let wholeText = textNode.data;
        let textIndex = textNode.data.indexOf(textToMakeNode);
        if (textIndex !== 0) {
            BoundNode._insertBefore(
                document.createTextNode(wholeText.substring(0, textIndex)),
                textNode
            );
        }
        if (textIndex + textToMakeNode.length < wholeText.length) {
            BoundNode._insertAfter(
                document.createTextNode(wholeText.substring(textIndex + textToMakeNode.length)),
                textNode
            );
        }
        textNode.data = wholeText.substring(textIndex, textIndex + textToMakeNode.length);
        return textNode;
    }

    static _insertBefore(newNode, node) {
        return node.parentNode.insertBefore(newNode, node);
    }

    static _insertAfter(newNode, node) {
        return node.parentNode.insertBefore(newNode, node.nextSibling);
    }

    constructor(updateCb, expressions) {
        this.updateCb = updateCb;
        this.expressions = expressions;
    }

    update() {
        this.updateCb(this.expressions);
    }
}

class HTMLefeTemplate {
    static _randomId() {
        return new Array(4).fill(0).map(() => Math.random().toString(36).substr(2, 9)).join("-");
    }

    static _joinTemplateStrings(arr1, arr2) {
        return arr2.reduce((accu, current, i) => accu + current + arr1[i + 1], arr1[0])
    }

    static _createTemplateElement(templateStrings, expressionIds) {
        const templateHtml = HTMLefeTemplate._joinTemplateStrings(templateStrings, expressionIds);
        let templateTag = document.createElement("template");
        templateTag.innerHTML = templateHtml;
        // TODO: print warning if there is more than one child, and error if there are none
        if (templateTag.content.children.length > 1) {
            console.warn("HTMLefeTemplate: More than one child element in template, ignoring all other than the first")
        }
        return templateTag.content.firstElementChild;
    }

    constructor(templateStrings, expressionCallbacks) {
        this._expressionCbToExpression = new Map();
        this._expressions = expressionCallbacks.map(cb => {
            const newExpression = new Expression(HTMLefeTemplate._randomId(), cb);
            this._expressionCbToExpression.set(cb, newExpression);
            return newExpression;
        });

        this.element = HTMLefeTemplate._createTemplateElement(templateStrings, this._expressions.map(e => e.id));
        this._expressions.forEach(e => e.boundNode = BoundNode.CreateBoundNode(this.element, e));
    }

    updateExpressions(expressionCbs, onStartExecuting, onFinishExecuting) {
        let boundNodesToUpdate = new Set();

        // TODO: For debugging. remove this later
        let updatedValues = [];

        for (let expressionCb of expressionCbs) {
            const expression = this._expressionCbToExpression.get(expressionCb);
            boundNodesToUpdate.add(expression.boundNode);

            onStartExecuting(expression.cb);
            updatedValues.push(
                expression.execute()
            );
            onFinishExecuting(expression.cb);
        }

        for (let boundNode of boundNodesToUpdate) {
            boundNode.update()
        }

        console.log("Updated ", updatedValues);
        console.log("Binding structure: ", this)
    }
}


function _htmlefet(propsObject) {
    return (strings, ...expressions) => {
        const template = new HTMLefeTemplate(strings, expressions);

        if (typeof propsObject === "object") {
            /** TODO: Problem with watch is that a re-evaluation won't be queued if a parameter wasn't accessed before,
             *  and if some other conditional was changed but it's not watched, then unexpected behavior can happen.
             *  consider the example:
             *  <div>${() => window.shouldKill ? props.something : props.somethingElse}</div>
             *  a rerender shouldnt happen when window.shouldKill is changed, but it should happen if props.something
             *  changed, because window.shouldKill could have caused it to be accessed...
             */
            watch(
                propsObject,
                key => isExecutingExpression && propsUsedInsideExpression.add(key),
                (key, value) => {
                    // TODO: Check if value is different than the current one, and spare expression evaluations
                    changedPropsList.add(key);
                    render();
                }
            );
        }


        let propsToExpressionCbs = {};
        let isExecutingExpression = false;
        let changedPropsList = new Set();
        let propsUsedInsideExpression = new Set();


        const updateExpressions = exs => {
            /**
             * Executes expressions, while monitoring accessed props
             */
            template.updateExpressions(
                exs,
                () => isExecutingExpression = true,
                expression => {
                    isExecutingExpression = false;

                    propsUsedInsideExpression.forEach(propName => {
                        if (!propsToExpressionCbs.hasOwnProperty(propName)) {
                            propsToExpressionCbs[propName] = new Set();
                        }
                        propsToExpressionCbs[propName].add(expression)
                    });
                    propsUsedInsideExpression.clear();
                }
            )
        };

        const render = _.throttle(() => {
            // Determine which expressions contain changed props
            let expressionsToExecute = new Set();
            for (let changedProp of changedPropsList) {
                let expressionsWithChangedProps = propsToExpressionCbs[changedProp];
                if (expressionsWithChangedProps) {
                    for (let expression of propsToExpressionCbs[changedProp]) {
                        expressionsToExecute.add(expression);
                    }
                } else {
                    console.warn(`A prop changed but no expression is linked to it: ${changedProp}`)
                }
            }
            changedPropsList.clear();
            updateExpressions([...expressionsToExecute])

        }, 1000 / 60, {leading: true});

        // Initial render
        updateExpressions(expressions);

        return template.element
    }
}


function rompstomp(propsObject, templateCb, evalCb) {
    /**
     * This executes on the users LexicalContext each time a template is created.
     * It creates a new htmlefet from user supplied templateCb, bound to propsObject, and returns the bound element.
     */
    let htmlefetInstance = new efet();
    let runHtmLefet = htmlefetInstance._htmlefet(propsObject);
    return htmlefetInstance.termplate(evalCb || (t => eval(t)), templateCb, "runHtmLefet");
}

function efet() {
    /**
     * This is the exported object, its value is meant to be eval'd.
     * When eval'd, `rompstomp` runs, and does the following:
     *      - creates `htmlefet` instance, containing _htmlefet and termplate refs
     *      -
     * @type {function(*=): function(*=, ...[*]=): *}
     * @private
     */
    efet.prototype._htmlefet = _htmlefet;
    efet.prototype.termplate = termplate;
    return `(${rompstomp.toString()})`;
}

// Exporting explicit htmlefet because rompstomp evals `new htmlefet()`, so that must be the name of the function.
export {efet}